Feature: Search functionality of Google

	  Scenario Outline: Google Search Test Scenario
    Given URL of Google
    When User search for <Search> in search box
    Then User arrives on the result page <Result>
    Examples:
    |Search|Result|
    |"Bahamas"|"Bahamas"|
    |"Amsterdam"|"Amsterdam"|