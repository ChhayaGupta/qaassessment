Feature: feature to test Expedia search functionality for travel
  
  Scenario: Look for travel option on Expedia
    Given I navigate to the Expedia website
    When I look for flight and accomodation
    Then The result page shows travel option for chosen destination