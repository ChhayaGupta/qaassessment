package stepDefination;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GoogleSteps {
	
	WebDriver driver;
	
	@Given("^URL of Google$")
	public void url_of_Google() {
		//Setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver","C:\\Eclipse\\chromedriver.exe");
		//Creating an object of ChromeDriver
		driver = new ChromeDriver();
		//Maximize the window
		driver.manage().window().maximize();
		//Deleting all the cookies
		driver.manage().deleteAllCookies();
		//Specify pageLoadTimeout and Implicit wait
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//launching the Google application
		driver.get("https://www.google.com");
	    
	}

	@When("^User search for \"([^\"]*)\" in search box$")
	public void user_search_for_in_search_box(String Search) {
		//Search
		driver.findElement(By.name("q")).sendKeys(Search);
		//driver.findElement(By.name("btnK")).click();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		WebElement element = driver.findElement(By.name("btnK"));
		js.executeScript("arguments[0].click();", element);
	}

	@Then("^User arrives on the result page \"([^\"]*)\"$")
	public void user_arrives_on_the_result_page(String Result) throws IOException {
		//Check for result page
		if (driver.getPageSource().contains("Result")){
	        System.out.println("Search Passed!");
	      //Capture screenshot and store it in the Screenshots folder
	        Date d = new Date();
			System.out.println(d.toString());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
			TakesScreenshot ts = (TakesScreenshot)driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source, new File("./Screenshots/Screen" +sdf.format(d) +".png"));
	    } else {
	        System.out.println("Search Failed");
	    }
		driver.close();
	}

}
