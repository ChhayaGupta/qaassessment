package runner;

import cucumber.api.junit.Cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="src/test/resources/features",
		glue= {"stepDefination"},
		plugin = {"pretty",
        "json:reports/cucumber.json",
        "html:reports/cucumber"}
)

public class TestRunner {

}
