package stepDefination;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ExpediaSteps {
	WebDriver driver;
	@Given("^I navigate to the Expedia website$")
	public void i_navigate_to_the_Expedia_website() {
		//Setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver","C:\\Eclipse\\chromedriver.exe");
		//Creating an object of ChromeDriver
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		//Deleting all the cookies
		driver.manage().deleteAllCookies();
		//Specify pageLoadTimeout and Implicit wait
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//launching the Expedia application
		driver.get("https://www.expedia.com");   
	}

	@When("^I look for flight and accomodation$")
	public void i_look_for_flight_and_accomodation() {
		try {
			driver.findElement(By.xpath("//span[text()='Stays']")).click();
			driver.findElement(By.xpath("//label[text()='Add a flight']")).click();
			// Leaving from
			Thread.sleep(5000);
			driver.findElement(By.xpath("//button[@aria-label = 'Leaving from']")).click();
			driver.findElement(By.id("location-field-origin")).sendKeys("Brussels");
			Thread.sleep(4000);
			List<WebElement> drp = driver.findElements(By.xpath("//div[@class='uitk-menu-container elevation-8 uitk-menu-open uitk-menu-pos-left']//ul[@class='uitk-typeahead-results no-bullet']/li"));
			System.out.println(drp.get(1).getText());
			drp.get(1).click();
			
			// Going to
			Thread.sleep(5000);
			driver.findElement(By.xpath("//button[@aria-label = 'Going to']")).click();
			driver.findElement(By.id("location-field-destination")).sendKeys("New York");
			Thread.sleep(5000);
			List<WebElement> drp1 = driver.findElements(By.xpath("//div[@class='uitk-menu-container elevation-8 uitk-menu-open uitk-menu-pos-left']//ul[@class='uitk-typeahead-results no-bullet']/li"));
			System.out.println(drp1.get(2).getText());
			drp1.get(2).click();
					
			//Check-in and Check-out
			driver.findElement(By.xpath("//button[@id='d1-btn']")).click();
			Thread.sleep(5000);
			driver.findElement(By.xpath("//button[@class='uitk-new-date-picker-day'][contains(@aria-label,'Dec 21, 2020')]")).click();
			driver.findElement(By.xpath("//button[@class='uitk-new-date-picker-day'][contains(@aria-label,'Dec 27, 2020')]")).click();
			driver.findElement(By.xpath("//span[text()='Done']")).click();
			
			//Travelers
			Thread.sleep(5000);
			driver.findElement(By.xpath("//button[contains(text(),'1 room, 2 travelers')]")).click();
			//driver.findElement(By.xpath("//a[@aria-label='Travelers']")).click();
			
			// Select one Adult
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("svg[aria-labelledby*='uitk-step-decrease-adults']")).click();
						
			//Select 1 Child of age 3
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("svg[aria-labelledby*='uitk-step-increase-children']")).click();
			WebElement age_drpdn = driver.findElement(By.id("child-age-input-0-0"));
			Select age_dd = new Select(age_drpdn);
			age_dd.selectByIndex(3);
			
			//Click on Done
			driver.findElement(By.xpath("//button[text()='Done']")).click();		
			
			//Click on Search
			driver.findElement(By.xpath("//button[contains(text(),'Search')]")).click();
			
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}

	@Then("^The result page shows travel option for chosen destination$")
	public void the_result_page_shows_travel_option_for_chosen_destination()  {
		System.out.println("Result page shows Brussels to New York travel option");
		//Closes the browser window
		driver.close();
	    
	}

}
