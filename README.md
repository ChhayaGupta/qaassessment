# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What things to install in order to run the automation script ###

* Install Eclipse as IDE
* Install Cucmber Eclipse Plugin
* Create new Maven Project
* Under created project - In pom.xml, download all the dependencies for Selenium webdriver and Cucmber to build the project
* Download chromedriver.exe

### Under Maven Project folder ###

##### Go to src/test/resources folder #####
* Create 'features' folder  
* Create 'Expedia.feature' and 'Google.feature' file under features folder
* After writing scenarios in feature file, Run as Cucumber feature

##### Go to src/test/java folder #####
* Create 'runner' package  
* Create 'TestRunner.java' class under runner package
* Create 'stepDefination' package
* Create 'ExpediaSteps.java' and 'GoogleSteps.java' under stepDefination package
* Run 'TestRunner.java' and it will execute Expedia and Google appication.

### Postman ###
* Create Collection in Postman
* Create Get request to check weather in New York using API from openweather.org
* Go to Test tab and add a test on the max_temperature



